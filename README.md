# springboot-punchcards

#### 介绍
打卡系统

#### 软件架构
springboot+mybatis+mysql+maven


#### 安装教程

1.  执行 `git clone https://gitee.com/jiazhi99/punchcards.git`将代码拉到本地
2.  直接运行即可,写的很简单,可以根据自己具体需求做一个小的程序,此代码只是一个demon

#### 项目说明

仅仅是一个简单的springboot项目,适用于初学springboot起步的程序