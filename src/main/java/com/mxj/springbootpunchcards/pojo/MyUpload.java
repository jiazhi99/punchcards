package com.mxj.springbootpunchcards.pojo;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class MyUpload {
    private String name;
    private String age;
    private String birthday;
    private MultipartFile file;
}
