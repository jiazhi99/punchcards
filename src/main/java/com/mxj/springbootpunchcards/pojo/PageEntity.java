package com.mxj.springbootpunchcards.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageEntity<T> {
    private Long totalPage;
    private List<T> data;
}
