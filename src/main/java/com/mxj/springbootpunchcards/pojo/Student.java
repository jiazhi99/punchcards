package com.mxj.springbootpunchcards.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "student")
public class Student implements Serializable {
    @Id
    private int id;
    private String name;
    private String department;
    private String classtype;
    private String timerange;
    private String temperature;
    private String todaydate;
    private String health;
    private String sex;
    private String describes;
}
