package com.mxj.springbootpunchcards.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "admin")
public class User {
    @Id
    private int id;
    private String username;
    private String password;
}
