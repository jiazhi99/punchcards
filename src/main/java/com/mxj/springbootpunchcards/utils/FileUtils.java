package com.mxj.springbootpunchcards.utils;

import com.sun.org.apache.bcel.internal.generic.NEW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@Slf4j
public class FileUtils {
    public static String fileInputStream(String url, MultipartFile file) {
        if (file.isEmpty()) {
            return "00";
        }
        String fileName = file.getOriginalFilename();
        File dest = new File(url+ "/" + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest); // 保存文件
            return "200";
        } catch (Exception e) {
            e.printStackTrace();
            return "500";
        }
    }
}
