package com.mxj.springbootpunchcards.utils;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.mxj.springbootpunchcards.pojo.Student;
import org.apache.poi.hssf.usermodel.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

@lombok.extern.java.Log
public class ExcleUtils {
    private static final String MORNING = "清晨(8点-8点半)";
    private static final String AFTERNOON = "下午(14点-14点半)";
    private static final String EVENING = "晚上(18点-18点半)";

    public void excelWrite(List<Student> studentList, String timerange, HttpServletResponse response) throws UnsupportedEncodingException {
        LogFactory logFactory = LogFactory.create();
        Log log = logFactory.getLog(ExcleUtils.class);
        //在内存中传建一个Excel文件(工作簿)
//        XSSFWorkbook excel = new XSSFWorkbook();
        HSSFWorkbook excel = new HSSFWorkbook();
        Student student = new Student();
        if (!studentList.isEmpty()) {
            if (timerange.equals(MORNING)) {
                //创建一个工作表对象
                HSSFSheet sheet = excel.createSheet("晨检体温日报统计表");
                //在工作表中创建行对象
                HSSFRow title = sheet.createRow(0);
                //在行中创建单元格对象 在第一行
                title.createCell(0).setCellValue("日期");
                title.createCell(1).setCellValue("系别");
                title.createCell(2).setCellValue("班级");
                title.createCell(3).setCellValue("姓名");
                title.createCell(4).setCellValue("晨检腋下体温（8：00--8：30");
                //文字居中
                excel.createCellStyle().setAlignment(HSSFCellStyle.ALIGN_CENTER);
                //设置列宽
                sheet.setColumnWidth(0, 2500);
                sheet.setColumnWidth(1, 4500);
                sheet.setColumnWidth(4, 7500);
                //写入List<List<String>>中的数据
                int rowIndex = 1;
                for (Student student1 : studentList) {
                    student = student1;
                    //创建一个row行，然后自增1
                    HSSFRow row = sheet.createRow(rowIndex);
                    HSSFCell cell0 = row.createCell(0);
                    HSSFCell cell1 = row.createCell(1);
                    HSSFCell cell2 = row.createCell(2);
                    HSSFCell cell3 = row.createCell(3);
                    HSSFCell cell4 = row.createCell(4);
                    //将内容对象的文字内容写入到单元格中
                    cell0.setCellValue(student.getTodaydate());
                    cell1.setCellValue(student.getDepartment());
                    cell2.setCellValue(student.getClasstype());
                    cell3.setCellValue(student.getName());
                    cell4.setCellValue(student.getTemperature());
                    rowIndex++;
                }
                //准备将Excel的输出流通过response输出到页面下载
                //八进制输出流
                response.setContentType("application/vnd.ms-excel;charset=utf-8");
                //这后面可以设置导出Excel的名称，此例中名为student.xls
                String filename = student.getClasstype() + student.getTodaydate().substring(1, 5) + "晨报表.xls";
                String encodeFileName = URLEncoder.encode(filename, "UTF-8");
                response.setHeader("Content-disposition", "attachment;filename=" + encodeFileName + ";" + "filename*=utf-8''" + encodeFileName);

                //刷新缓冲
                try {
                    response.flushBuffer();
                    //workbook将Excel写入到response的输出流中，供页面下载
                    excel.write(response.getOutputStream());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (timerange.equals(AFTERNOON)) {
                //创建一个工作表对象
                HSSFSheet sheet = excel.createSheet("午检体温日报统计表");
                //在工作表中创建行对象
                HSSFRow title = sheet.createRow(0);
                //在行中创建单元格对象 在第一行
                title.createCell(0).setCellValue("日期");
                title.createCell(1).setCellValue("系别");
                title.createCell(2).setCellValue("班级");
                title.createCell(3).setCellValue("姓名");
                title.createCell(4).setCellValue("午检腋下体温（14：00--14：30）");
                //文字居中
                excel.createCellStyle().setAlignment(HSSFCellStyle.ALIGN_CENTER);
                //设置列宽
                sheet.setColumnWidth(0, 2500);
                sheet.setColumnWidth(1, 4500);
                sheet.setColumnWidth(4, 7500);
                //写入List<List<String>>中的数据
                int rowIndex = 1;
                for (Student student1 : studentList) {
                    student = student1;
                    //创建一个row行，然后自增1
                    HSSFRow row = sheet.createRow(rowIndex);
                    HSSFCell cell0 = row.createCell(0);
                    HSSFCell cell1 = row.createCell(1);
                    HSSFCell cell2 = row.createCell(2);
                    HSSFCell cell3 = row.createCell(3);
                    HSSFCell cell4 = row.createCell(4);
                    //将内容对象的文字内容写入到单元格中
                    cell0.setCellValue(student.getTodaydate());
                    cell1.setCellValue(student.getDepartment());
                    cell2.setCellValue(student.getClasstype());
                    cell3.setCellValue(student.getName());
                    cell4.setCellValue(student.getTemperature());
                    rowIndex++;
                }
                //准备将Excel的输出流通过response输出到页面下载
                //八进制输出流
                response.setContentType("application/vnd.ms-excel;charset=utf-8");
                //这后面可以设置导出Excel的名称，此例中名为student.xls
                String filename = student.getClasstype() + student.getTodaydate().substring(1, 5) + "午报表.xls";
                String encodeFileName = URLEncoder.encode(filename, "UTF-8");
                response.setHeader("Content-disposition", "attachment;filename=" + encodeFileName + ";" + "filename*=utf-8''" + encodeFileName);
                //刷新缓冲
                try {
                    response.flushBuffer();
                    //workbook将Excel写入到response的输出流中，供页面下载
                    excel.write(response.getOutputStream());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (timerange.equals(EVENING)) {
                //创建一个工作表对象
                HSSFSheet sheet = excel.createSheet("晚检体温日报统计表");
                //在工作表中创建行对象
                HSSFRow title = sheet.createRow(0);
                //在行中创建单元格对象 在第一行
                title.createCell(0).setCellValue("日期");
                title.createCell(1).setCellValue("系别");
                title.createCell(2).setCellValue("班级");
                title.createCell(3).setCellValue("姓名");
                title.createCell(4).setCellValue("晚检腋下体温（18：00--18：30）");
                //文字居中
                excel.createCellStyle().setAlignment(HSSFCellStyle.ALIGN_CENTER);
                //设置列宽
                sheet.setColumnWidth(0, 2500);
                sheet.setColumnWidth(1, 4500);
                sheet.setColumnWidth(4, 7500);
                //写入List<List<String>>中的数据
                int rowIndex = 1;
                for (Student student1 : studentList) {
                    //创建一个row行，然后自增1
                    student = student1;
                    HSSFRow row = sheet.createRow(rowIndex);
                    HSSFCell cell0 = row.createCell(0);
                    HSSFCell cell1 = row.createCell(1);
                    HSSFCell cell2 = row.createCell(2);
                    HSSFCell cell3 = row.createCell(3);
                    HSSFCell cell4 = row.createCell(4);
                    //将内容对象的文字内容写入到单元格中
                    cell0.setCellValue(student.getTodaydate());
                    cell1.setCellValue(student.getDepartment());
                    cell2.setCellValue(student.getClasstype());
                    cell3.setCellValue(student.getName());
                    cell4.setCellValue(student.getTemperature());
                    rowIndex++;
                }
                //准备将Excel的输出流通过response输出到页面下载
                //八进制输出流
                response.setContentType("application/vnd.ms-excel;charset=utf-8");
                //这后面可以设置导出Excel的名称，此例中名为student.xls
                String filename = student.getClasstype() + student.getTodaydate().substring(1, 5) + "晚报表.xls";
                String encodeFileName = URLEncoder.encode(filename, "UTF-8");
                response.setHeader("Content-disposition", "attachment;filename=" + encodeFileName + ";" + "filename*=utf-8''" + encodeFileName);
                //刷新缓冲
                try {
                    response.flushBuffer();
                    //workbook将Excel写入到response的输出流中，供页面下载
                    excel.write(response.getOutputStream());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
