package com.mxj.springbootpunchcards.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GetTimeInDay {
    //得到今天剩余秒数
    public static int getLastSeconds(){
        Calendar calendar= Calendar.getInstance();
        // 得到今天 晚上的最后一刻 最后时间
        String last=getTime()+" 23:59:59";
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            // 转换为今天
            Date latDate=sdf.parse(last);
            // 得到的毫秒 除以1000转换 为秒 然后将秒换成小时
            return (int)(latDate.getTime()-System.currentTimeMillis())/1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
    //
    public static String getTime(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date());
    }
    public static void main(String[] args) {
        System.out.println(getLastSeconds());
    }
}
