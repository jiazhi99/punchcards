package com.mxj.springbootpunchcards;

import com.mxj.springbootpunchcards.pojo.PageResult;
import com.mxj.springbootpunchcards.utils.ExcleUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages = "com.mxj.springbootpunchcards.dao")
public class SpringbootPunchcardsApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootPunchcardsApplication.class, args);
        System.out.println("-----------项目启动完成-------------");
    }
    @Bean
    public ExcleUtils excleUtils(){
        return new ExcleUtils();
    }
}
