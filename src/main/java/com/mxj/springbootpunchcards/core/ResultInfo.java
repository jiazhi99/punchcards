package com.mxj.springbootpunchcards.core;

import java.io.Serializable;

/**
 * 这个类是专门返回后端判断出来的信息的类
 */
public class ResultInfo implements Serializable {
    private boolean flag;   //判断持久层是否将数据查出
    private Object data;   //data用来将数据存储
    private String Msg;  //errorMsg是用来表示后端给前端错误信息的响应

    public ResultInfo() {
    }

    public ResultInfo(boolean flag, Object data, String Msg) {
        this.flag = flag;
        this.data = data;
        this.Msg = Msg;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        this.Msg = msg;
    }

    @Override
    public String toString() {
        return "ResultInfo{" +
                "flag=" + flag +
                ", data=" + data +
                ", Msg='" + Msg + '\'' +
                '}';
    }
}
