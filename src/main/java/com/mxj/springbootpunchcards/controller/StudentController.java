package com.mxj.springbootpunchcards.controller;


import cn.hutool.crypto.digest.BCrypt;
import com.mxj.springbootpunchcards.core.ResultInfo;
import com.mxj.springbootpunchcards.pojo.MyUpload;
import com.mxj.springbootpunchcards.pojo.PageResult;
import com.mxj.springbootpunchcards.pojo.Student;
import com.mxj.springbootpunchcards.pojo.User;
import com.mxj.springbootpunchcards.service.StudentService;
import com.mxj.springbootpunchcards.utils.ExcleUtils;
import com.mxj.springbootpunchcards.utils.FileUtils;
import com.mxj.springbootpunchcards.utils.GetTimeInDay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/student")
@Slf4j
public class StudentController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private ExcleUtils excleUtils;

    //打卡页跳转
    @RequestMapping("/punchCard")
    public ModelAndView punchCard() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("student");
        return modelAndView;
    }

    //手机端打卡页面
    @RequestMapping("/punchCardM")
    public ModelAndView punchCardM() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("studentTow");
        return modelAndView;
    }

    //登录页面跳转
    @RequestMapping("/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    /**
     * 后台登录首页跳转
     * @return
     */
    @RequestMapping("/banckIndex")
    public ModelAndView banckIndex() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("banck");
        return modelAndView;
    }

    /**
     * 打卡入库
     * @param student
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/addInformation", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResultInfo addInfomation(@RequestBody Student student, HttpServletRequest request, HttpServletResponse response) {
        String uniqueId = BCrypt.hashpw(student.getName() + student.getTimerange(), BCrypt.gensalt());
        Cookie cookie = new Cookie("uniqueIdCookie", uniqueId);
        cookie.setPath("/");
        cookie.setMaxAge(GetTimeInDay.getLastSeconds());
        log.warn("cookie过期时间还剩" + GetTimeInDay.getLastSeconds()+"秒");
        try {
            response.addCookie(cookie);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("cookie响应超时...");
            return new ResultInfo(false, null, "cookie响应超时,请联系管理员!");
        }
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            log.info("" + cookies);
            for (int i = 0; i < cookies.length; i++) {
                String value = cookies[i].getValue();
                String name = cookies[i].getName();
                if (name.equals("uniqueIdCookie") && value != null) {
                    if (BCrypt.checkpw(student.getName() + student.getTimerange(), value)) {
                        return new ResultInfo(false, null, "您已重复提交,请下次再来吧!");
                    }
                }
            }
        }
        boolean flag = false;
        try {
            log.info("接收到的参数: " + student.getName());
            flag = studentService.addInfomation(student);
            if (flag) {
                return new ResultInfo(flag, null, "恭喜您，提交成功！！！");
            } else {
                return new ResultInfo(flag, null, "提交失败! 重试");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultInfo(flag, null, "对不起,服务器丢啦");
        }
    }

    /**
     * 导出excel表
     * @param todaydate
     * @param timerange
     * @param classtype
     * @param response
     * @return
     */
    @RequestMapping("/downLoadExl")
    @ResponseBody
    public ResultInfo downLoadExl(@RequestParam("todaydate") String todaydate, @RequestParam("timerange") String timerange, @RequestParam("classtype") String classtype, HttpServletResponse response) {
        List<Student> studentList = studentService.downLoadExl(todaydate, timerange, classtype);
        log.info("查询到数据" + studentList);
        try {
            excleUtils.excelWrite(studentList, timerange, response);
            return new ResultInfo(true, null, "成功下载");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultInfo(false, null, "服务器异常");
        }
    }

    /**
     * 登录页面
     * @param username
     * @param password
     * @param request
     * @return
     */
    @RequestMapping("/loginByUsernameAndPassword")
    @ResponseBody
    public ResultInfo loginUser(String username, String password, HttpServletRequest request) {
        try {
            List<User> userList = studentService.loginByUsernameAndPassword(username, password);
            if (!userList.isEmpty() && userList.size() > 0) {
                request.getSession().setAttribute("user", userList.get(0));
                return new ResultInfo(true, null, "登录成功,3秒后进行跳转!");
            } else {
                return new ResultInfo(true, null, "登录失败,账户或密码错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultInfo(true, null, "登录失败,服务器异常");
        }
    }

    //管理员后端数据查询
    @RequestMapping("/showStudent")
    @ResponseBody   //@RequestParam("currentPage")   @RequestParam("pagesize")
    public ResultInfo showStudent(Integer currentPage, Integer pagesize) {
        PageResult pageResult = studentService.findAll(currentPage, pagesize);
        if (pageResult.getRows() != null && pageResult.getRows().size() != 0) {
            log.info("总数:" + pageResult.getTotal() + "数据: " + pageResult.getRows());
            return new ResultInfo(true, pageResult, "查询成功");
        } else {
            return new ResultInfo(false, null, "查询失败");
        }
    }

    /**
     * 删除数据
     * @return
     */
    @GetMapping("/{id}")
    public ResultInfo deleteStudent(@PathVariable("id") Integer id){
        Integer integer = studentService.deleteStudent(id);
        if (integer>0){
            return new ResultInfo(true,null,"删除成功");
        }else {
            return new ResultInfo(false, null, "删除失败");
        }
    }
    //上传文件跳转
    @RequestMapping("/uploadHtml")
    public ModelAndView uploadHtml() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("upload");
        return modelAndView;
    }
    /**
     * 文件上传
     * @param myUpload
     * @return
     */
    @PostMapping("/upload")
    @ResponseBody
    public ResultInfo upload(MyUpload myUpload){
        MultipartFile file = myUpload.getFile();
        String originalFilename = file.getOriginalFilename();
        String url = "G:\\projectTestFile\\springboot-punchcards\\src\\main\\resources\\static\\file";  //此处写绝对路径
        String flag = FileUtils.fileInputStream(url, file);   //此方法进行文件上传
        if ("200".equals(flag)){
            log.info(originalFilename+"文件上传成功 ; "+"上传路径为 "+url+"/"+originalFilename);
        }else {
            log.error("上传错误,错误代码: "+flag);
            return new ResultInfo(true,"","上传错误,错误代码: "+flag);
        }
        return new ResultInfo(true,"","上传成功");
    }
}
