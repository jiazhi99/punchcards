package com.mxj.springbootpunchcards.service;

import com.mxj.springbootpunchcards.pojo.PageEntity;
import com.mxj.springbootpunchcards.pojo.PageResult;
import com.mxj.springbootpunchcards.pojo.Student;
import com.mxj.springbootpunchcards.pojo.User;

import java.util.List;

public interface StudentService {
    boolean addInfomation(Student student);
    //查询出所有数据
    List<Student> downLoadExl(String todaydate, String timerange, String classtype);

    // 登录
    List<User> loginByUsernameAndPassword(String username , String password);

    //管理员登录用户显示
    PageResult findAll(Integer currentPage4 , Integer pagesize);

    //删除单条数据
    Integer deleteStudent(Integer id);
}
