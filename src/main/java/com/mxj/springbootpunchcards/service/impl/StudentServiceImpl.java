package com.mxj.springbootpunchcards.service.impl;


import com.mxj.springbootpunchcards.dao.StudentRepository;
import com.mxj.springbootpunchcards.dao.UserLogin;
import com.mxj.springbootpunchcards.pojo.PageResult;
import com.mxj.springbootpunchcards.pojo.Student;
import com.mxj.springbootpunchcards.pojo.User;
import com.mxj.springbootpunchcards.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository repository;
    @Autowired
    private UserLogin userLogin;

    /**
     * 进行温度打卡提交数据
     * @param student
     * @return
     */
    @Override
    public boolean addInfomation(Student student) {
        //存入数据库
        int index = repository.insertSelective(student);
        if (index >= 0) {
            return true;
        }
        return false;
    }

    @Override
    public Integer deleteStudent(Integer id) {
        try {
            Integer index = repository.deleteStudent(id);
            return index;
        } catch (Exception e) {
            log.error("系统异常,原因如下: "+e.getMessage());
            return 0;
        }
    }

    /**
     * 导出excel表格
     * @param todaydate
     * @param timerange
     * @param classtype
     * @return
     */
    @Override
    public List<Student> downLoadExl(String todaydate, String timerange, String classtype) {
//        Student student = new Student();
        List<Student> studentList = repository.selectStudent(todaydate, timerange, classtype);
        return studentList;
    }

    /**
     * 分页查询  列表数据
     * @param currentPage4
     * @param pagesize
     * @return
     */
    @Override
    public PageResult findAll(Integer currentPage4, Integer pagesize) {
        List<Student> studentList = repository.selectAll();
        List<Student> students = repository.selectStudentByPage((currentPage4-1)*pagesize+1,pagesize);
        PageResult pageResult = new PageResult((long)studentList.size(),students);
        return pageResult;
    }


    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    @Override
    public List<User> loginByUsernameAndPassword(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        List<User> userList = userLogin.select(user);
        return userList;
    }

}
