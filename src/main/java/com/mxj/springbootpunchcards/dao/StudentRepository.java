package com.mxj.springbootpunchcards.dao;

import com.mxj.springbootpunchcards.pojo.Student;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface StudentRepository extends Mapper<Student> {
    @Select("select * from student where todaydate= #{todaydate} and timerange= #{timerange} and classtype= #{classtype}")
    List<Student> selectStudent(@Param("todaydate") String todaydate, @Param("timerange") String timerange, @Param("classtype") String classtype);

    @Select("select * from student limit #{currentPage4} , #{pagesize}")
    List<Student> selectStudentByPage(@Param("currentPage4") int currentPage4,@Param("pagesize") int pagesize);

    //删除单条数据
    @Delete("delete from student where id = #{id,jdbcType=INTEGER}")
    Integer deleteStudent(Integer id);
}
