package com.mxj.springbootpunchcards.dao;

import com.mxj.springbootpunchcards.pojo.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserLogin extends Mapper<User> {
}
