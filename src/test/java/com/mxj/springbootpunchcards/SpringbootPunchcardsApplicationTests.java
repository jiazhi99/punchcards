package com.mxj.springbootpunchcards;

import lombok.extern.java.Log;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

@SpringBootTest
@Log
class SpringbootPunchcardsApplicationTests {

    @Test
    void contextLoads() {
//        //在内存中传建一个Excel文件(工作簿)
//        XSSFWorkbook excel = new XSSFWorkbook();
//        //创建一个工作表对象
//        XSSFSheet sheet = excel.createSheet("晨检体温日报统计表");
//        //在工作表中创建行对象
//        XSSFRow title = sheet.createRow(0);
//        //在行中创建单元格对象 在第一行
//        title.createCell(0).setCellValue("日期");
//        title.createCell(1).setCellValue("系别");
//        title.createCell(2).setCellValue("班级");
//        title.createCell(2).setCellValue("姓名");
//        title.createCell(2).setCellValue("晨检腋下体温（8：00--8：30）");
//        //创建行数据 在第二行
//        XSSFRow dateRow = sheet.createRow(1);
//        dateRow.createCell(0).setCellValue("贾智");
//        dateRow.createCell(1).setCellValue("山西大同");
//        dateRow.createCell(1).setCellValue("山西大同");
//        dateRow.createCell(2).setCellValue("24");
//        dateRow.createCell(2).setCellValue("24");
//        dateRow.createCell(2).setCellValue("24");
//        //从内存中将excel持久化到硬盘
//        FileOutputStream fileOutputStream = null;
//        try {
//            fileOutputStream = new FileOutputStream(new File("G:\\工作空间.xlsx"));
//            if (fileOutputStream != null) {
//                excel.write(fileOutputStream);
//                excel.close();
////                System.out.println("表格创建成功");
//                log.info("表格创建成功");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Test
    public void test01(HttpServletResponse response) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("信息表");

//        List<Teacher> classmateList = teacherservice.teacherinfor();

        String fileName = "userinf" + ".xls";//设置要导出的文件的名字
        //新增数据行，并且设置单元格数据

        int rowNum = 1;

        String[] headers = {"学号", "姓名", "身份类型", "登录密码"};
        //headers表示excel表中第一行的表头

        HSSFRow row = sheet.createRow(0);
        //在excel表中添加表头

        for (int i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }

        //在表中存放查询到的数据放入对应的列
//        for (Teacher teacher : classmateList) {
            HSSFRow row1 = sheet.createRow(rowNum);
            row1.createCell(0).setCellValue("贾智");
            row1.createCell(1).setCellValue("山西大同");
            row1.createCell(2).setCellValue("山西大同");
            row1.createCell(3).setCellValue("24");
//            rowNum++;
//        }

//        response.setContentType("application/octet-stream");
//        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
//        response.flushBuffer();
//        workbook.write(response.getOutputStream());
//            workbook.write();
    }
}
